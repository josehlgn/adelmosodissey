﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigo : MonoBehaviour
{
    private Animator animator;
    private GameObject Player;
    private Player player;
    private float velocidad = .5f;
    public float x = 0;
    public float distancia;
    public float minDist = .5f;
    public float maxDist = 4f;
    public bool _collision = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.gameObject.GetComponent<Animator>();
        Player = GameObject.FindGameObjectWithTag("Player");
        player = Player.GetComponent<Player>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManagerController.instance.getGameStates() == GameStates.playing)
        {
            distancia = Vector3.Distance(new Vector3(player.transform.position.x, player.transform.position.y, 0), transform.position);
            var desplazamiento = new Vector3(x, 0);
            if (distancia < 3f && distancia > minDist)
            {
                _collision = false;
                if (!_collision)
                {
                    if (Player.transform.position.x < transform.position.x)
                    {
                        x = -1;
                    }
                    else
                    {
                        x = 1;
                    }
                    animator.SetFloat("Horizontal", desplazamiento.x);
                    transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, velocidad * Time.deltaTime);
                }
                
            }
            if (_collision)
            {
                animator.SetFloat("Horizontal", 0);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            _collision = true;
        }
    }


}
