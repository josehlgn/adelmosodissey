﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cyclope : MonoBehaviour
{
    private Animator animator;
    private Rigidbody2D _rigidbody;
    private GameObject Player;
    private Player player;
    MovimientoEnemigo enemigo;
    private AudioSource _audio;

    public int vida = 50;
    public bool puedeAtacar = true;
    public bool atacando = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.gameObject.GetComponent<Animator>();
        _audio = this.gameObject.GetComponent<AudioSource>();
        _rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
        Player = GameObject.FindGameObjectWithTag("Player");
        player = Player.GetComponent<Player>();
        enemigo = gameObject.GetComponent<MovimientoEnemigo>();
    }

    private void Update()
    {
        UIManager.vidaBoss(vida);
    }

    private void FixedUpdate()
    {
       if (enemigo._collision && puedeAtacar)
        {
            if (Player.transform.position.x > transform.position.x)
            {
                animator.SetBool("AtaqueDerecha", true);
            }
            else
            {
                animator.SetBool("AtaqueIzquierda", true);
            }
            _audio.Play();
            atacando = true;
            puedeAtacar = false;
            StartCoroutine(coolDown());
        }
        if (vida <= 0)
        {
            animator.SetBool("Muerte", true);
            StartCoroutine(muerte());
        }
    }

    private IEnumerator coolDown()
    {
        yield return new WaitForSeconds(2);
        animator.SetBool("AtaqueDerecha", false);
        animator.SetBool("AtaqueIzquierda", false);
        atacando = false;
        puedeAtacar = true;
    }

    private IEnumerator muerte()
    {
        yield return new WaitForSeconds(1);
        UnityEngine.Object.Destroy(this.gameObject);
        UIManager.win();
    }

    private IEnumerator damageOff()
    {
        yield return new WaitForSeconds(1);
        animator.SetBool("Damage", false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Lanza" && player.atacando)
        {
            Arma arma = collision.gameObject.GetComponent<Arma>();
            animator.SetBool("Damage", true);
            this.vida -= arma.damage;
            if (collision.transform.position.x<transform.position.x)
            {
                _rigidbody.AddForce(Vector2.right * 2f, ForceMode2D.Impulse);
            } else
            {
                _rigidbody.AddForce(Vector2.left * 2f, ForceMode2D.Impulse);
            }
            
            StartCoroutine(damageOff());
        }
    }
}
