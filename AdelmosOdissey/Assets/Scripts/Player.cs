﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    // Referencias
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private Rigidbody2D _rigidbody;
    // private AudioSource audioSource; Sonido. Preguntar a Miguel


    // Atributos del jugador
    public int vida = 30;
    public float fuerzaSalto = 3f;
    public float distanciaRC = .5f;
    public float tiempoCooldownAtaque = 2f;
    public float velocidad = 2f;
    public float tiempoRespawn = 5f;
    public int vecesSalto = 0;
    public bool doblesalto = false;
    public bool puedeDefender = true;// Hacer que se pueda defender
    public bool defendiendo = false;
    public bool puedeAtacar = true;
    public bool atacando = false;
    public bool puedeSaltarDoble = false;
    private Vector3 posicionInicial;
    private ManagerScenes managerEscenas;
    private AudioSource _audio;

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        } catch(System.NullReferenceException)
        {
            Debug.Log("SpriteRenderer not found!!");
        }
        animator = this.gameObject.GetComponent<Animator>();
        _rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
        _audio = this.gameObject.GetComponent<AudioSource>();
        posicionInicial = transform.position;
        UIManager.updateVida(vida);
        GameObject escena = GameObject.FindGameObjectWithTag("GameController");
        managerEscenas = escena.GetComponent<ManagerScenes>();
        puedeSaltarDoble = GameManagerController.instance.getDobleSaltoActivado();
        vida = GameManagerController.instance.getVidaPersonaje();
    }

    private void Update()
    {
        UIManager.updateVida(vida);
        if (!puedeSaltarDoble)
        {
            puedeSaltarDoble = GameManagerController.instance.getDobleSaltoActivado();
        }
    }

    private void FixedUpdate()
    {
        if (GameManagerController.instance.getGameStates() == GameStates.playing)
        {
            Debug.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y - distanciaRC), Color.blue);

            var desplazamiento = new Vector3(Input.GetAxis("Horizontal"), 0);
            transform.position += desplazamiento * velocidad * Time.deltaTime;
            animator.SetFloat("Horizontal", desplazamiento.x);

            if (Input.GetKeyDown(KeyCode.W))
            {
                if (haySuperficie())
                {
                    salto();
                    if (puedeSaltarDoble) doblesalto = true;
                } else
                {
                    if (doblesalto)
                    {
                        doblesalto = false;
                        salto();
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (puedeAtacar)
                {
                    ataque();
                    _audio.Play();
                }
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (puedeDefender)
                {
                    defender();
                }
            }
            if (vida <= 0)
            {
                animator.SetBool("Muerte", true);
                jugadorMuerto();
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                UIManager.pausa();
            }
        }
    }

    private void jugadorMuerto()
    {
        animator.SetBool("Muerte", false);
        GameManagerController.setGameStates(GameStates.gameOver);
        Debug.Log(GameManagerController.instance.vidasTotales);
        if(GameManagerController.instance.vidasTotales > 0)
        {
            vida = 30;
            transform.position = posicionInicial;
        } else
        {
            UnityEngine.Object.Destroy(this.gameObject);
        }
   }

    private IEnumerator cooldownAtaque()
    {
        yield return new WaitForSeconds(tiempoCooldownAtaque);
        animator.SetBool("Ataque", false);
        atacando = false;
        puedeAtacar = true;
    }

    private IEnumerator cooldownDefensa()
    {
        yield return new WaitForSeconds(2);
            animator.SetBool("Defensa", false);
            defendiendo = false;
            puedeDefender = true;
    }

    private IEnumerator damageOff()
    {
        yield return new WaitForSeconds(1);
        animator.SetBool("Damage", false);
    }

    private bool haySuperficie()
    {
        vecesSalto = 0;
        return (Physics2D.Raycast(_rigidbody.position, Vector2.down, distanciaRC, (1 << LayerMask.NameToLayer("Suelo") | (1 << LayerMask.NameToLayer("Pinchos")))));
    }

    private void salto()
    {
        _rigidbody.AddForce(Vector3.up * fuerzaSalto, ForceMode2D.Impulse);
    }

    private void ataque()
    {
        animator.SetBool("Ataque", true);
        atacando = true;
        puedeAtacar = false;
        StartCoroutine(cooldownAtaque());
    }

    private void defender() //Metodo para la defensa
    {
        animator.SetBool("Defensa", true);
        defendiendo = true;
        puedeDefender = false;
        StartCoroutine(cooldownDefensa());
    }

    private IEnumerator berserker()
    {
        yield return new WaitForSeconds(10);
        if (vida>30)
        {
            vida = 30;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ciclope" && !defendiendo && !atacando)
        {
            Cyclope ciclope = collision.gameObject.GetComponent<Cyclope>();
            if (ciclope.atacando)
            {
                animator.SetBool("Damage", true);
                vida -= 5;
                StartCoroutine(damageOff());
            }
        }
        if (collision.gameObject.tag == "Slime" && !defendiendo && !atacando)
        {
            Slime slime = collision.gameObject.GetComponent<Slime>();
            if (slime.atacando)
            {
                animator.SetBool("Damage", true);
                vida -= 1;
                StartCoroutine(damageOff());
            }
        }
        if (collision.gameObject.tag == "Minotauro" && !defendiendo && !atacando)
        {
            Minotauro ms = collision.gameObject.GetComponent<Minotauro>();
            if (ms.puedeAtacar)
            {
                animator.SetBool("Damage", true);
                vida -= 3;
                StartCoroutine(damageOff());
            }
        }
        if (collision.gameObject.tag == "Pinchos")
        {
            animator.SetBool("Damage", true);
            vida -= 15;
            StartCoroutine(damageOff());
        }
        if (collision.gameObject.tag == "Pocion")
        {
            vida = 50;
            UIManager.ponerLogro("Has conseguido el logro, EL BERSERKER");
            StartCoroutine(berserker());
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "Vacio")
        {
            jugadorMuerto();
        }
        if (collision.gameObject.tag == "Pilares")
        {
            GameManagerController.instance.setVidaPersonaje(vida);
            managerEscenas.saltarEscena("Nivel 2");
        }
        if (collision.gameObject.tag == "Ruinas")
        {
            GameManagerController.instance.setVidaPersonaje(vida);
            managerEscenas.saltarEscena("Nivel 3");
        }
        if (collision.gameObject.tag == "cofre")
        {
            UIManager.ponerLogro("Has desbloqueado el doble salto, Felicidades!");
            GameManagerController.instance.setDobleSaltoActivado();
            puedeSaltarDoble = true;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "PocionAtaque")
        {
            UIManager.ponerLogro("Has conseguido la pocion de ataque, ahora quitas 5");
            GameManagerController.instance.setDañoArma(5);
            Destroy(collision.gameObject);
        }
    }
}

