﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public int damage = 2;

    // Start is called before the first frame update
    void Start()
    {
        damage = GameManagerController.instance.getDañoArma();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManagerController.instance.getDañoArma() != damage)
        {
            damage = GameManagerController.instance.getDañoArma();
        }
    }
}
