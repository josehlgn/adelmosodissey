﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectoParallax : MonoBehaviour
{
    private float lenght;
    private float startpos;
    public GameObject _camera;
    public float parallaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        startpos = transform.position.x;
        lenght = GetComponent<SpriteRenderer>().bounds.size.x;
    }
    
    private void FixedUpdate()
    {
        float temp = (_camera.transform.position.x * (1 - parallaxEffect));
        float dis = (_camera.transform.position.x * parallaxEffect);
        transform.position = new Vector3(startpos + dis, transform.position.y, transform.position.z);
        if (temp > startpos + lenght) startpos += lenght;
        else if (temp < startpos - lenght) startpos -= lenght;
    }
}
