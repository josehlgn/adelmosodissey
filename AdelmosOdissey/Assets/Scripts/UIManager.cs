﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{

    static UIManager current;
    //vida hp
    public TextMeshProUGUI vidaText;
    //logro
    public TextMeshProUGUI logroTexto;
    //Activar o desactivar logro
    public GameObject logroGameObject;
    //vida ciclope
    public TextMeshProUGUI vidaCiclope;
    // activar UI batalla ciclope
    public GameObject batallaBoss;
    //respawns restantes
    public TextMeshProUGUI respawnsRestantes;
    public GameObject pausaGameObject;

    public GameObject winObject;

    public GameObject GameOver;

    private ManagerScenes managerEscenas;

    private void Awake()
    {
        if (current != null && current != this)
        {
            Destroy(gameObject);
            return;
        }

        current = this;
        DontDestroyOnLoad(gameObject);
    }

    public static void updateVida(int vida)
    {
        if (current == null) return;
        current.vidaText.text = vida.ToString();
    }

    public static void ponerLogro(string texto)
    {
        if (current == null) return;
        current.logroTexto.text = texto;
        current.logroGameObject.SetActive(true);
        current.StartCoroutine(current.apagarLogro());
    }

    public IEnumerator apagarLogro()
    {
        yield return new WaitForSeconds(3);
        current.logroGameObject.SetActive(false);
    }

    public static void empiezabatallaBoss()
    {
        if (current == null) return;
        current.batallaBoss.SetActive(true);
    }

    public static void vidaBoss(int vidaBoss)
    {
        if (current == null) return;
        current.vidaCiclope.text = vidaBoss.ToString();
    }

    public static void respawnsUpdate(int respawns)
    {
        if (current == null) return;
        current.respawnsRestantes.text = respawns.ToString();
    }

    public static void pausa()
    {
        current.pausaGameObject.SetActive(true);
        GameManagerController.setGameStates(GameStates.pause);
    }

    public void seguirJuego()
    {
        current.pausaGameObject.SetActive(false);
        GameManagerController.setGameStates(GameStates.playing);
    }

    public void menu()
    {
        current.pausaGameObject.SetActive(false);
        managerEscenas.saltarEscena("Menu");
    }

    public void salir()
    {
        managerEscenas.salir();
    }

    public static void win()
    {
        current.winObject.SetActive(true);
        current.StartCoroutine(current.finaljuego(GameStates.gameWin));
    }

    public static void lose()
    {
        current.GameOver.SetActive(true);
    }

    private IEnumerator finaljuego(GameStates state )
    {
        yield return new WaitForSeconds(3);
        if (state == GameStates.gameOver)
        {
            current.batallaBoss.SetActive(false);
            current.GameOver.SetActive(false);
            GameManagerController.setGameStates(GameStates.gameOver);
        } else
        {
            current.batallaBoss.SetActive(false);
            current.winObject.SetActive(false);
            GameManagerController.setGameStates(GameStates.gameWin);
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameObject escena = GameObject.FindGameObjectWithTag("GameController");
        managerEscenas = escena.GetComponent<ManagerScenes>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagerController.instance.vidasTotales <=0)
        {
            Destroy(gameObject);
        }
    }
}
