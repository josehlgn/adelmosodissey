﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates
{
    playing,
    pause,
    gameOver,
    gameWin
}

public class GameManagerController : MonoBehaviour
{
    public GameStates gameStates;
    public static GameManagerController instance;
    private float _timeScale = 1.0f;
    private ManagerScenes managerEscenas;

    // Variables del jugador
    public int vidasTotales = 3;
    public bool dobleSaltoActivado = false;
    public int vidaPersonaje = 30;
    public int daño = 2;
    private AudioSource _audio;

    private void Awake()
    {
        if (instance != null && instance!= this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        UIManager.respawnsUpdate(vidasTotales);
        managerEscenas = this.GetComponent<ManagerScenes>();
        Scene escenaActiva = SceneManager.GetActiveScene();
        if (escenaActiva.name == "Nivel 2")
        {
            dobleSaltoActivado = true;
        } 
        if (escenaActiva.name == "Nivel 3")
        {
            daño = 5;
            UIManager.empiezabatallaBoss();
            dobleSaltoActivado = true;
        }
        _audio = this.gameObject.GetComponent<AudioSource>();
        _audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = _timeScale;
    }

    public void setDañoArma(int dañoSet)
    {
        daño = dañoSet;
    }

    public int getDañoArma()
    {
        return daño;
    }

    public GameStates getGameStates()
    {
        return this.gameStates;
    }

    public bool getDobleSaltoActivado()
    {
        return dobleSaltoActivado;
    }

    public void setDobleSaltoActivado()
    {
        dobleSaltoActivado = true;
    }

    public void setVidaPersonaje(int vida)
    {
        vidaPersonaje = vida;
    }

    public int getVidaPersonaje()
    {
        return vidaPersonaje;
    }

    public static void setGameStates(GameStates gameStates)
    {
        instance.gameStates = gameStates;
        gameStatesUpdate();
    }

    static void gameStatesUpdate()
    {
        switch (instance.gameStates)
        {
            case GameStates.pause:
                instance.setGameInPause();
                break;
            case GameStates.gameOver:
                instance.setGameInOver();
                break;
            case GameStates.gameWin:
                instance.setGameInWin();
                break;
            default:
                instance.setGameInPlaying();
                break;
        }
    }

    private void setGameInPause()
    {
        _timeScale = 0;
        instance.gameStates = GameStates.pause;
    }

    private void setGameInOver()
    {
        vidasTotales -= 1;
        if (vidasTotales > 0)
        {
            Debug.Log("Has muerto, te quedan " + vidasTotales + " vidas");
            UIManager.respawnsUpdate(vidasTotales);
            setGameStates(GameStates.playing);
        }
        else
        {
            managerEscenas.saltarEscena("Menu");
            _audio.Stop();
            Destroy(gameObject);
            Debug.Log("Ni las bolas de drac te resucitan");
        }
    }

    private void setGameInWin()
    {
        managerEscenas.saltarEscena("Menu");
        _audio.Stop();
        Destroy(gameObject);
    }

    private void setGameInPlaying()
    {
        _timeScale = 1f;
        instance.gameStates = GameStates.playing;
    }
}
